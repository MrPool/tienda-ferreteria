<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PdfController extends Controller
{
  private function total()
  {
    $cart = \Session::get('cart');
    $total = 0;
    foreach($cart as $item){
      $total += $item->price * $item->quantity;
    }

    return $total;
  }

  public function invoice()
  {
      $cart = \Session::get('cart');
      $total = $this->total();
      //return view('store.order-detail', compact('cart', 'total'));
      $date = date('Y-m-d');
      $view =  \View::make('store.partials.pdf', compact('date', 'cart', 'total'))->render();
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      return $pdf->stream('pdf');
  }


}
