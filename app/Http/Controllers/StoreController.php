<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Products2;

class StoreController extends Controller
{
    public function index()
    {
    	$products = Products2::all();
    	//dd($products);
    	return view('store.index', compact('products'));
    }

    public function show($slug)
    {
    	$product = Product::where('slug', $slug)->first();
    	//dd($product);

    	return view('store.show', compact('product'));
    }

    public function productos1()
    {

    	$products = Product::where('category_id', 1)->get();
    	//dd($products);
    	return view('store.productos', compact('products'));
    }

    public function productos2()
    {
      $products = Product::where('category_id', 2)->get();
      //dd($products);
      return view('store.productos', compact('products'));
    }

    public function productos3()
    {
      $products = Product::where('category_id', 3)->get();
      //dd($products);
      return view('store.productos', compact('products'));
    }

    public function productos4()
    {
      $products = Product::where('category_id', 4)->get();
      //dd($products);
      return view('store.productos', compact('products'));
    }

    public function contact()
    {
      return view('store.contact');
    }

    public function about()
    {
      return view('store.about');
    }
}
