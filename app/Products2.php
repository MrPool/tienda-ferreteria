<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products2 extends Model
{
      protected $table = 'products2s';

    protected $fillable = ['name', 'slug', 'description', 'extract', 'image', 'visible', 'price', 'category_id'];


    // Relation with Category
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    // Relation with OrderItem
    public function order_item()
    {
        return $this->hasOne('App\OrderItem');
    }
}
