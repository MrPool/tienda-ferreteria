<?php
return array(
    // set your paypal credential
    'client_id' => 'AT-QkjQjA-4Cu2uj059bA1Bw6y9PCNQc10a46h-9OkKD8fhj2Xmh6McXipvSbkZm76sM6yHWQLovmNF9',
    'secret' => 'EDkQd7V7s3X8UDOOLYj-noJOLTRKSYDDNjgMThQL1r8n8xppsRcqVWufRDjQWIO_-ftySl8Z9Y_C8p7L',

    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);
