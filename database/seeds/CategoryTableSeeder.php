<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use  Faker\Factory as Faker;

use App\Category;

class CategoryTableSeeder extends Seeder {

	/**
	 * Run the Categories table seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();
		$data = array(
			[
				'name' => 'Lámparas de mano',
				'slug' => 'lamparas-de-mano',
				'description' => $faker->text($maxNbChars = 100),
				'color' => $faker->hexcolor
			],
      [
				'name' => 'Lamparas de Minería',
				'slug' => 'mineria',
				'description' => $faker->text($maxNbChars = 100),
				'color' => $faker->hexcolor
			],
			[
				'name' => 'Lámparas de campamento',
				'slug' => 'campamento',
				'description' => $faker->text($maxNbChars = 100),
				'color' => $faker->hexcolor
			],
			[
				'name' => 'Accesorios',
				'slug' => 'accesorios',
				'description' => $faker->text($maxNbChars = 100),
				'color' => $faker->hexcolor
			]
		);

		Category::insert($data);

	}
}
