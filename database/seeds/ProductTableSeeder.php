<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use  Faker\Factory as Faker;

use App\Product;

class ProductTableSeeder extends Seeder {

	/**
	 * Run the Products table seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();
		$data = array(
			[
				'name' 			=> 'STINGER LED HL',
				'slug' 			=> 'stinger-led-hl',
				'description' 	=> 'Multi-function On/Off push-button switch lets you choose three lighting modes and strobe. Three modes and strobe:
High for a super-bright beam - 640 lumens; 22,000 candela peak beam intensity; 297 meter beam distance; runs 1.25 hours
Medium for bright light and longer run times – 340 lumens; 11,000 candela peak beam intensity; 210 meter beam distance; runs 2 hours
Low for light without glare and extended run times – 170 lumens; 5,500 candela peak beam intensity; 148 meter beam distance; runs 4 hours
Strobe for disorienting or signaling your location; runs 3 hours. Deep-dish parabolic reflector produces a concentrated beam with optimum peripheral illumination. C4® LED technology, impervious to shock with a 50,000 hour lifetime
Optimized electronics provide regulated intensity throughout battery charge
3-cell, 3.6 Volt Ni-MH sub-C battery, rechargeable up to 1000 times
10 hr. steady charge, 2.5 hr. fast charge, or PiggyBack charger options
Machined aircraft aluminum body with non-slip rubberized comfort grip; anti-roll rubber ring included
IPX4 water-resistant; 1 meter impact resistance tested
Unbreakable polycarbonate lens with scratch-resistant coating
8.41” (21.36 cm); 12.4 oz (352 g)
Serialized for positive identification
Limited lifetime warranty
Assembled in USA',
				'extract' 		=> 'When you need maximum illumination with a wide beam to search a large area, the Stinger LED HL rechargeable, high lumen flashlight provides a blast of 640 lumens with 297 meters of beam distance.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/stinger-led-hl_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'STINGER HPL',
				'slug' 			=> 'stinger-hpl',
				'description' 	=> 'Multi-function push-button switch lets you choose three lighting modes and strobe
Three modes and strobe:
High for a, far-reaching beam with maximum illumination: 740 lumens; 438m beam; 48,000 candela; runs 1.25 hours
Medium for bright light and longer run time: 380 lumens; 310m beam; 24,000 candela; runs 2.5 hours
Low provides extended run times for when a less intense beam is desired: 190 lumens; 219m beam; 12,000 candela; runs 4.5 hours
Strobe for disorienting or signaling: runs 3 hours
Deep-dish parabolic reflector produces a long-range targeting beam with optimum peripheral illumination
C4® LED technology, impervious to shock with a 50,000 hour lifetime
Regulated run time provides consistent performance throughout battery life
3-cell, 3.6 Volt NiMH sub-C battery, rechargeable up to 1000 times.
10 hr. steady charge, 2.5 hr. fast charge, or PiggyBack charger options
Machined aircraft aluminum body with non-slip rubberized comfort grip
IPX4 water-resistant; 3 meter impact resistance tested
Unbreakable polycarbonate lens with scratch-resistant coating
9.23 in. (23.44 cm); 13.6 oz. (385 grams)
Serialized for positive identification
Limited lifetime warranty
Assembled in USA',
				'extract' 		=> 'The Stinger HPL is a long-range, hand-held flashlight that produces a wide, 740 lumen beam pattern with brighter peripheral illumination and superior down-range performance.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/stinger-hpl_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'STINGER LED',
				'slug' 			=> 'stinger-led',
				'description' 	=> 'Multi-function On/Off push-button switch lets you choose three lighting modes and strobe; designed for extremely long life; tested at 1 million actuations
Three modes and strobe:
- High for a bright super-bright beam - 350 lumens; 24,000 candela peak beam intensity; 310 meter beam distance; runs 2 hours
- Medium for bright light and longer run times – 175 lumens; 12,000 candela peak beam intensity; 219 meter beam distance; runs 3.75 hours
- Low for light without glare and extended run times – 85 lumens; 6,000 candela peak beam intensity; 155 meter beam distance; runs 7.25 hours
- Strobe for disorienting or signaling your location; runs 5.5 hours
Deep-dish parabolic reflector produces a concentrated beam with optimum peripheral illumination
C4® LED technology, impervious to shock with a 50,000 hour lifetime
Regulated run time provides consistent performance throughout battery life
3-cell, 3.6 Volt Ni-Cd sub-C battery, rechargeable up to 1000 times.
3-cell, 3.6 Volt Ni-MH sub-C battery, rechargeable up to 1000 times.
10 hr. steady charge, 2.5 hr. fast charge, or PiggyBack charger options
Machined aircraft aluminum body with non-slip rubberized comfort grip
IPX4 water-resistant; 1 meter impact resistance tested
Unbreakable polycarbonate lens with scratch-resistant coating
Includes anti-roll rubber ring
Length: 8.41 in.; Weight: 12.4 oz
Serialized for positive identification
Limited lifetime warranty
Assembled in USA',
				'extract' 		=> 'This all purpose flashlight is designed for the broadest range of lighting needs at the best value.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/stingerled.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'POLYSTINGER LED HAZ-LO',
				'slug' 			=> 'plystinger-led-haz-lo',
				'description' 	=> 'Shockproof C4® LED Technology delivers 15,000 candela peak beam intensity.
3 modes:
High – 15,000 candela peak beam intensity, 130 Lumens, 4 hour runtime
Low – 5700 candela peak beam intensity, 50 Lumens, 12 hour runtime
Moonlight Mode – 120 candela peak beam intensity, 1.3 Lumens, 20 Day runtime
Super-tough, non-conductive chemical and corrosion-resistant nylon polymer. Developed for petrochemical and heavy industrial applications with hazardous atmospheres
NiCd battery is rechargeable up to 1000 times
Multi-function push-button switch tested to 1 million cycles
Unbreakable polycarbonate lens with scratch-resistant coating wears through spills and maintains clarity in difficult work environments
Deep-dish parabolic reflector produces a tight beam with optimum peripheral illumination
3 meter impact-resistance tested
IPX4 rated for water-resistance
Serialized for positive identification
10.2 in. (25.9 cm); 13.5 oz (382 grams) with battery
Available in black or yellow
Limited lifetime warranty
Assembled in USA',
				'extract' 		=> 'High Performance Intrinsically Safe Class I, Div. 1 Stinger® Flashlight. Developed for hazardous locations, atmospheres and environments including petro-chem, utility, and other heavy industrial applications.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/polystinger-led-hazlo_yellow.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'POLYSTINGER DS LED',
				'slug' 			=> 'polystinger-ds-led',
				'description' 	=> 'Super-tough, non-conductive nylon polymer with non-slip rubberized comfort grip. Unbreakable polycarbonate lens with scratch-resistant
coating. Available in black.
Length: 8.64 in. (21.95 cm); Diameter: Major Diameter: 1.76 in. (4.47 cm); Body Diameter: 1.21 in. (3.07 cm); 11.9 oz (338 grams).
Multi-function, easy access head mounted push-button switch designed for extremely long life; tested at 1 million actuations and multi-function
push-button tactical tailswitch where both switches offer full feature control independent of each other.',
				'extract' 		=> 'Designed for the broadest range of lighting needs at the best val ue.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/polystinger-led.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'PORTABLE SCENE LIGHT',
				'slug' 			=> 'portable-scene-light',
				'description' 	=> '
Six C4® LED and wide reflectors produce a uniform flood pattern
Three user-selectable outputs:
High for a super-bright flood beam: 5,300 lumens; 410m beam; runs 4 hours
Medium for an intense beam and longer run time: 2,500 lumens; 292m beam; runs 9 hours
Low for when a less intense beam is ideal and for longer run time: 1,300 lumens; 215m beam; runs 18 hours
Selectable diffuser settings for two beam widths
Optimum peripheral illumination for scene lighting; 90° swivel neck allows you to aim the beam where you need it for task lighting
Sealed lead acid batteries, rechargeable up to 500 times
Batteries will continue charge while operating directly from an AC or DC power source so you can be confident that you will always have a light when you need it
Indefinite run time on AC or DC external power with the supplied power cords
Integral D-rings allow attachment of the included shoulder strap
Design allows for the light to be used in confined spaces and rugged terrain
Balanced design and easy setup:
Less than 30 seconds for full deployment
Pole extends to 72”
Cord built into the pole to avoid snags
Stabilization legs provide balance on uneven surfaces
Lead acid batteries provide ballast and stability
25 lbs (11.3 kg) provides weight stabilization, but is light enough to carry
Packs to a compact size (22”; 55.88cm) for easy stowage
High-impact thermoplastic housing; weatherproof construction
IP67 rated; dust-tight and waterproof to 1 meter for 30 minutes.
Designed to resist 40mph wind continuous in all directions with head fully extended
Available in Safety Yellow with AC and DC charge cords and shoulder strap
Assembled in USA
Limited lifetime warranty',
				'extract' 		=> 'We have made the Portable Scene Light even brighter – it now produces 5,300 lumens! Rapidly deployed and easily stowed, it features a narrow footprint and 72" extension pole, letting you bring this light almost anywhere - from wide open places to tight, confined spaces.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/scene-light_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 3
			],
			[
				'name' 			=> '2AA PROPOLYMER',
				'slug' 			=> '2aa-propolymer',
				'description' 	=> 'Powered by 2 "AA" alkaline batteries
One (1) 0.5 Watt High-Flux LED, 30,000 hour lifetime in parabolic reflector module.
20 Lumens typical. Parabolic reflector produces a focused beam with optimum peripheral illumination.
Runs up to 20 continuous hrs.
Dust and water ingress protected to IP57; in accordance with specification EN60529:1992
O-Ring sealed construction
Impact resistant housing – drop test verified
6.3"
3.6 oz.
Available in black and yellow
Assembled in USA',
				'extract' 		=> 'Tough, super-convenient, extra bright flashlight with a push button tail switch.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/propolymer-2aa-led.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'PROTAC HL USB',
				'slug' 			=> 'protac-hl-usb',
				'description' 	=> 'TEN-TAP® Programming – Choice of three user selectable programs:
1.) high/strobe/low; 2.) high only; 3.) low/medium/high
High for maximum illumination: 850 lumens; 10,000 candela; 200m beam; runs 1.5 hours
Medium for bright light and longer run time: 350 lumens; 4,000 candela; 126m beam; runs 4 hours
Low for a less intense beam and extended run time: 85 lumens; 1,000 candela; 63m beam; runs 12 hours
Strobe for signaling or disorienting: runs 1.5 hours
Uses lithium ion battery (P/N 74175); also accepts two 3V CR123A lithium batteries or a commercially available Li-Ion 18650 rechargeable battery
Optional Streamlight 18650 Charger Kit and batteries available
Self-adjusting battery cradle secures multiple battery sizes and eliminates battery rattle
Rechargeable using supplied USB cord or optional AC wall adapter
Durable, anodized aluminum construction; IPX4 water resistant; 1 meter impact resistance tested
Water-resistant sliding metal sleeve protects USB port
Includes USB cord, removable pocket clip and tear resistant nylon holster
Multi-function, push-button tail switch for one-handed operation
C4® LED technology, impervious to shock with a 50,000 hour lifetime
Anti-roll head prevents the light from rolling away when you set it down
Removable pocket clip; nylon holster included
IPX4 water-resistant; 1 meter impact resistance tested
6.50” (16.5 cm); 7.2 oz. (204g) with Li-Ion rechargeable battery (included in purchase)
RoHS compliant
Limited lifetime warranty',
				'extract' 		=> 'High lumen tactical light that is USB rechargeable, so you can charge on-the-go using almost any USB source. It also accepts multiple battery sources to use as a back up so you’ll always have power when you need it.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/protac-hl-usb_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'PROTAC HL',
				'slug' 			=> 'protac-hl',
				'description' 	=> 'C4® LED technology, impervious to shock with a 50,000 hour lifetime
TEN-TAP® Programming – Choice of three user selectable programs:
1.) high/strobe/low; 2.) high only; 3.) low/high
High for maximum illumination: 1,100 lumens; 36,000 candela; runs 2 hours
Low a less intense beam and longer run time: 35 lumens; 1,200 candela; runs 43 hours
Strobe for disorienting or signaling: runs 2 hours
Multi-function, push-button tail switch for one-handed operation
Optimized electronics provides thermal management and regulated intensity
Solid State power regulation provides maximum light output throughout battery life
Durable anodized machined aircraft aluminum construction
Anti-roll head prevents the light from rolling away when you set it down
Removable pocket clip
O-ring sealed glass lens
IPX7; waterproof to 1 meter for 30 minutes; 1 meter impact resistance tested
Includes three 3V CR123A lithium batteries and nylon holster
7.10 in. (18.03 cm); 9.3 oz (263 grams) with batteries
RoHS compliant
Limited lifetime warranty',
				'extract' 		=> 'The technology that allows us to put 1,100 lumens in the palm of your hands now provides longer run times. The ProTac HL 3 provides extraordinary brightness with a wide beam pattern that lights your entire area and runs two hours on high beam.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/protac-hl-3_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'TWIN-TASK 3C-UV LED',
				'slug' 			=> 'twin-task-3c-uv-led',
				'description' 	=> 'C4® LED technology, with a 50,000 hour lifetime. Six 390nm UV LEDs with 100,000 hour lifetime.
Unbreakable polycarbonate lens.
3 modes:
C4 LED – 3730 candela peak beam intensity, 185 Lumens, 27 hours runtime
6 UV LED – 40mW, 63 hours runtime
3 UV LED - 30mW, 81 hours runtime
Three "C" size alkaline batteries.
Push-button sequences through C4® LED, six 390nm UV LEDs, three 390nm UV LEDs, and off.
IPX4 rated for water-resistant operation. All openings O-ring sealed. One meter impact resistance tested. Wrist lanyard.
L x W x D: 9.02" x 1.68" x 1.26" (22.9 x 4.26 x 3.2 cm)
16 oz (449 g)
Matte Black',
				'extract' 		=> 'The TT-3C-UV flashlight features three lighting modes and the latest in LED technology. The six 390nm UV LEDs provide ultraviolet light for detection of fradulent documents or detecting engine and HVAC leaks. The white C4® power LED with its textured reflector provides an even beam, along with piercing hotspot for distance.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/tt3c-uv-led_angle1.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'TWIN-TASK 3AA LED',
				'slug' 			=> 'twin-task-3aa-led',
				'description' 	=> 'C4® LED technology, with a 50,000 hour lifetime. Three ultra-bright white LEDs with 100,000 hour lifetime.
Unbreakable polycarbonate lens.
2 modes:
C4 LED – 2283 candela peak beam intensity, 100 Lumens, 5.75 hours runtime
3 LED – 467 candela peak beam intensity, 33 Lumens, 34 hours runtime
Three "AA" size alkaline batteries.
Push-button sequences through C4® LED, three ultra-bright LEDs, C4 LED, and off.
IPX4 rated for water-resistant operation. All openings O-ring sealed. One meter impact resistance tested. Wrist lanyard.
L x W x D: 8.54" x 1.33" x .79" (21.7 x 3.38 x 2.0 cm)
6.91 oz (196 g)
Matte Black',
				'extract' 		=> 'This versatile, long-running, go-anywhere light cuts through the dark with extreme brightness. It features dual lighting modes: A C4® LED that provides a bright, even beam with a piercing hotspot for distance, and three ultra-bright LEDs for area lighting and longer run time.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/tt3aa-led_angle1.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'TWIN-TASK 3AAA-LASER LED',
				'slug' 			=> 'twin-task-3aaa-laser-led',
				'description' 	=> 'Pre-focused visible red Laser cartridge. C4® LED technology, with a 50,000 hour lifetime. Five ultra-bright white LEDs with 100,000 hour lifetime.
Unbreakable glass lens.
2 modes:
C4 LED – 2000 candela peak beam intensity, 100 Lumens, 2.75 hours runtime
5 LED – 800 candela peak beam intensity, 68 Lumens, 5.25 hours runtime
Three "AAA" size alkaline batteries.
Push-button sequences through four positions — Laser only, C4® LED, Laser/C4® LED combo, or five ultra-bright white LEDs.
Usable range of 100 yards+ when using laser pointer only. 100 feet+ when in Laser/C4® LED combo position.
IPX4 rated for water-resistant operation. All openings O-ring sealed. One meter impact resistance tested. Wrist lanyard.
Length: 5.38 in. (13.69 cm) Major Diameter: 1.57 in. (3.98 cm) Body Diameter: 1.17 in. (2.98 cm)
7.04 oz (200 g)
Matte Black
ACCESSORIES
DOCS/INFO
PRODUCT RATINGS
',
				'extract' 		=> 'The TT-3AAA flashlight features a laser pointer and the latest in LED technology. The C4® power LED with its textured reflector provides an even beam, along with piercing hotspot for distance. Five ultra-bright LEDs provide area lighting and extended run time.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/tt3aaa-laser-led_angle1.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'MULTI OPS',
				'slug' 			=> 'multi-ops',
				'description' 	=> 'Shock-proof C4® LED design – 2-3x Brighter than a High Flux LED!
Light Output and Runtime:
C4® LED – Impervious to shock with a 50,000 hour lifetime; 2500 candela peak beam intensity; 50 lumens; 4.75 continuous hours to 10% of initial lumen output.
Five (5) 390nm UV LEDs – 40mw measured system output; 10 continuous hours to 10% of initial output power.
Visible Red Laser – 650nm, Class IIIa (3R), <5mW laser output power, useable laser range of 100+ feet when in LED/Laser mode, 100+ yards when in Laser Only mode; 40 continuous hours.
White C4 LED/Laser Mode – 4 continuous hours.
Powered by 3 "AAA" alkaline batteries (included)
Corrosion-resistant anodized aircraft aluminum construction
IPX4 Rated for water resistant operation; All openings o-ring or gasket sealed; 1 meter impact resistance tested; Rotary switch for pre-settable operation.
Holster and Lanyard included',
				'extract' 		=> 'COMPACT, MULTI-TASKING LIGHT W/ C4® LED, UV LEDs AND LASER POINTER',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/multi-ops.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'STRION LED HL',
				'slug' 			=> 'strion-led-hl',
				'description' 	=> 'Multi-function On/Off push-button tail switch lets you choose three lighting modes and strobe
Three modes and strobe:
- High for a super-bright beam - 500 lumens; 10,000 candela peak beam intensity; 200 meter beam distance; runs 1 hour
- Medium for bright light and longer run times – 250 lumens; 5,000 candela peak beam intensity; 141 meter beam distance; runs 2 hours
- Low for light without glare and extended run times – 125 lumens; 2,500 candela peak beam intensity; 100 meter beam distance; runs 3.5 hours
- Strobe for disorienting or signaling your location; runs 2.5 hours
C4® LED technology, impervious to shock with a 50,000 hour lifetime
Lithium ion battery is rechargeable up to 1000 times; fully recharges in 3 hrs.
Clamp style charger holder with digital control circuit that prevents over-charge; LED indicates charge status
PiggyBack charger doubles your run time by charging a spare battery (sold separately) so you can keep on working; reduces down time
Features a dual power input – USB port or “traditional” AC/DC charging
Integrated micro-USB interface specifically designed to guide the plug into place for a secure connection
USB port provides a convenient charging option using modern devices (power input only; does not charge USB devices)
IPX4 water-resistant; 2 meter impact resistance tested
6000 series machined aircraft aluminum with anodized finish
Borofloat high temperature glass lens
Anti-roll head prevents the light from rolling away when you set it down
Grooved barrel adapts to long gun mount
5.9" (14.99 cm); 5.2 oz. (148 grams)
Serialized for positive identification
Limited lifetime warranty',
				'extract' 		=> 'Get extraordinary brightness in a wide beam pattern with the Strion LED HL. This rechargeable, high lumen flashlight provides maximum illumination for searching a large area, with 500 lumens and 200 meters of beam distance.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/strion-led-hl_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'STRION HPL',
				'slug' 			=> 'strion-hpl',
				'description' 	=> 'Multi-function, push-button tactical tail switch lets you select three lighting modes and strobe
Three modes and strobe:
High for a far-reaching beam with maximum illumination: 615 lumens; 400m beam; 40,000 candela; runs 1 hour
Medium for bright light, good beam distance and longer run time: 320 lumens; 283m beam; 20,000 candela; runs 2 hours
Low provides a less intense beam and extended run time: 160 lumens; 200m beam; 10,000 candela; runs 3.75 hours
Strobe for disorienting or signaling: runs 2.5 hours
Deep-dish parabolic reflector provides a long-range targeting beam with optimum peripheral illumination
C4® LED technology, impervious to shock with a 50,000 hour lifetime
Optimized electronics provides regulated intensity
Lithium ion battery is rechargeable up to 1,000 times; fully recharges in 3 hrs.
Clamp style charger holder with digital control circuit that prevents over-charge; LED indicates charge status
PiggyBack charger doubles your run time by charging a spare battery (sold separately) so you can keep on working; reduces down time
Features a dual power input – USB port or “traditional” AC/DC charging
Integrated micro-USB interface specifically designed to guide the plug into place for a secure connection
USB port provides a convenient charging option using modern devices (power input only; does not charge USB devices)
IPX4 water-resistant; 2 meter impact resistance tested
6000 series machined aircraft aluminum with anodized finish
Unbreakable polycarbonate lens with scratch-resistant coating
Grooved barrel adapts to long gun mount
7" (17.78 cm); 7.1 oz. (202 grams)
Serialized for positive identification
Limited lifetime warranty
Assembled in the USA',
				'extract' 		=> 'The Strion HPL is a compact, long-range, hand-held flashlight that produces a wide, 615 lumen beam pattern with brighter peripheral illumination and superior down-range performance.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/strion-hpl_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'STRION LED',
				'slug' 			=> 'strion-led',
				'description' 	=> 'Multi-function, push-button tactical tail switch lets you select three lighting modes and strobe
Three modes and strobe:
- High for a bright super-bright beam: 260 lumens; 10,000 candela peak beam intensity; 200 meter beam distance; runs 2 hours
- Medium for bright light and longer run time:– 130 lumens; 5,000 candela peak beam intensity; 141 meter beam distance; runs 4 hours
- Low for light without glare and extended run time: 65 lumens; 2,500 candela peak beam intensity; 100 meter beam distance; runs 7.5 hours
- Strobe for disorienting or signaling your location; runs 5.5 hours
C4® LED technology, impervious to shock with a 50,000 hour lifetime
Optimized electronics provides regulated intensity
Lithium ion battery is rechargeable up to 1000 times; fully recharges in 3 hrs.
Clamp style charger holder with digital control circuit that prevents over-charge; LED indicates charge status
PiggyBack charger doubles your run time by charging a spare battery (sold separately) so you can keep on working; reduces down time
Features a dual power input – USB port or “traditional” AC/DC charging
Integrated micro-USB interface specifically designed to guide the plug into place for a secure connection
USB port provides a convenient charging option using modern devices (power input only; does not charge USB devices)
IPX4 water-resistant; 2 meter impact resistance tested
6000 series machined aircraft aluminum with anodized finish
Borofloat high temperature glass lens
Anti-roll head prevents the light from rolling away when you set it down
Grooved barrel adapts to long gun mount
5.9" (14.99 cm); 5.2 oz. (148 grams)
Serialized for positive identification
Limited lifetime warranty',
				'extract' 		=> 'The Strion LED is a compact, professional light designed for the broadest range of lighting needs at the best value. It provides an optimal balance of a bright, far-reaching beam and ample peripheral light, as well as long run times.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/strion-led.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'SCORPION HL',
				'slug' 			=> 'scorpion-hl',
				'description' 	=> 'C4® LED technology, impervious to shock with a 50,000 hour lifetime
TEN-TAP® Programming: Choice of three operating modes: 1.) high/strobe/low (factory default); 2.) high only; 3.) low/high
High: For maximum illumination to light up an entire area; 725 lumens; 18,500 candela; 272m beam distance; runs 1.25 hours
Low: For a less intense beam and longer run time for battery conservation; 35 lumens; 950 candela; 62m beam distance; runs 18 hours
Strobe: For disorienting or signaling; runs 2.5 hours
Electronic power regulation provides maximum light output throughout battery life; optimized electronics provide regulated intensity
Durable, anodized aluminum construction with enhanced rubber-armored sleeve; textured for a sure grip with gloved hands and in any environment; O-ring sealed glass lens
Anti-roll head design prevents the light from rolling away when you set it down
IPX7; waterproof to 1 meter for 30 minutes; 1 meter impact resistance tested
Powered by two 3V CR123A lithium batteries (included)
5.46 in. (13.87 cm); 4.8 oz (136 grams) with batteries
Assembled in USA
Limited lifetime warranty',
				'extract' 		=> 'Replicates the size, shape and feel that you love so much about the Scorpion series, but with new technology and a high lumen output (725 lumens). It also features TEN-TAP® Programming that gives users the choice of three operating modes.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/scorpion-hl_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'SCORPION X',
				'slug' 			=> 'scorpion-x',
				'description' 	=> 'C4® LED with high, low and strobe functions:
High: 200 lumens; 11,000 candela (peak beam intensity); runs 2.5 hours
Low: 10 lumens; 550 candela peak beam intensity; runs 50 hours
Strobe: runs 6 hours
Multi-function, push-button tactical tail switch
Durable, anodized aluminum construction with enhanced rubber-armored sleeve; textured for a sure grip with gloved hands and in any environment; O-ring sealed glass lens
LED Solid State Power Regulation provides maximum light output throughout battery life.
Anti-roll head design prevents the light from rolling away when you set it down
IPX4 rated design; water resistant operation; 3 meter impact resistance tested
Powered by two 3V CR123A lithium batteries (included) with a shelf life of ten years
Lithium Battery Notice under DOCS/INFO
5.6” (14.22 cm); 5.0 oz (142 g) with batteries
Assembled in USA
Limited lifetime warranty',
				'extract' 		=> 'The Scorpion X is a compact and lightweight tactical flashlight with high, low and strobe modes. This high-performance flashlight also features an enhanced rubber-armored sleeve for a secure grip in any environment.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/scorpion-x_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'ARGO HAZ-LO',
				'slug' 			=> 'argo-haz-lo',
				'description' 	=> 'Meets lighting needs to illuminate objects at a distance
Features one white C4® LED with two output modes:
Low for extended run time and when less intense light is needed: 20 lumens, 700 candela peak beam intensity; 53m beam distance; runs 50 hours
High for maximum output with a long-range beam distance: 90 lumens; 3,200 candela peak beam intensity; 113m beam distance; runs 8 hours
Push-button switch is easy to use when wearing gloves and is recessed to protect it from inadvertent activation
Powered by 3 “AAA” alkaline batteries (included)
90º tilting head features a robust ratcheting system; prevents neck fatigue
Durable, thermoplastic construction with elastomer over mold
IPX4 rated for water resistance; 2 meter impact resistance tested
Includes elastic head strap and rubber hard hat strap
5.4 oz. (153 g)',
				'extract' 		=> 'When you’re working in a hazardous environment and your task requires both hands, the Argo HAZ-LO provides 90 lumens of bright light with a far-reaching beam (113 meters) so you can safely see what’s ahead of you.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/argo-hazlo_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 2
			],
			[
				'name' 			=> 'ARGO',
				'slug' 			=> 'argo',
				'description' 	=> 'Meets lighting needs to illuminate objects at a distance
			Features one white C4® LED with three output modes:
			High for maximum output with a long-range beam distance: 150 lumens, 9,250 candela peak beam intensity, 3 hour run time, 190m beam distance
			Medium for intense brightness and longer run time: 100 lumens, 6,375 candela peak beam intensity, 4.5 hour run time, 160m beam distance
			Low for extended run times and when less intense light is needed: 45 lumens, 2,750 peak beam intensity, 30 hour run time, 103m beam distance
			Push-button switch is easy to use when wearing gloves and is recessed to protect it from inadvertent activation
			Powered by 3 “AAA” alkaline batteries (included)
			Low-level battery indicator visible in the facecap of each light
			Red LED flashes when battery voltage is low and batteries are near the end of their usable life
			LED is located in the reflector in between the “Low Batt” marking and functions while the light is on
			90º tilting head features a robust ratcheting system; prevents neck fatigue
			Durable, co-molded rubber body
			IPX4 rated for water resistance; 2 meter impact resistance tested
			Includes elastic head strap and rubber hard hat strap
			5.4 oz. (153 g)',
				'extract' 		=> 'If you often find yourself needing to illuminate objects at a distance, the long-range capability of the Argo delivers a far-reaching beam of 190 meters.
			You may prefer the features of this headlamp if you’re checking utility lines from the ground or moving about during outdoor activities and need to see where you’re going and what’s ahead',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/argo_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 2
			],
			[
				'name' 			=> 'TRIDENT HAZ-LO',
				'slug' 			=> 'trident-haz-lo',
				'description' 	=> 'Meets lighting needs for both close-up and distance work
			Features center C4® LED and three ultra-bright white 5mm LEDs with two output modes:
			3 ultra-bright white 5mm LEDs for intense brightness and longer run time: 30 lumens; 300 candela peak beam intensity; 35m beam distance; runs 24 hours
			C4 LED for super-bright light with strong beam distance: 85 lumens; 2,600 candela peak beam intensity; 102m beam distance; runs 8 hours
			Push-button switch is easy to use when wearing gloves and is recessed to protect it from inadvertent activation
			Powered by 3 “AAA” alkaline batteries (included)
			90º tilting head features a robust ratcheting system; prevents neck fatigue
			Durable, thermoplastic construction with elastomer over mold
			IPX4 rated for water resistance; 2 meter impact resistance tested
			Includes elastic head strap and rubber hard hat strap
			5.5 oz. (156 g)',
				'extract' 		=> 'When you need a Div. 1 headlight that provides you with the broadest range of lighting applications, the multi-purpose Trident HAZ-LO provides a good mix of light for both distance and up-close work.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/trident-hazlo_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 2
			],
			[
				'name' 			=> 'SUPER SIEGE',
				'slug' 			=> 'super-siege',
				'description' 	=> 'Multiple modes:
			White C4® LED:
			High for brightest light: 1,100 lumens; runs 5 hours
			Medium for bright light and longer run time: 550 lumens; runs 10.5 hours
			Low provides extended run time: 125 lumens; runs 35 hours
			4 Red LEDs (preserves night vision):
			High: 2.7 lumens; runs 110 hours
			Low: 1 lumen; runs 288 hours
			Flash SOS mode for emergency signaling: 2.7 lumens; runs 230 hours
			Polycarbonate cover provides even 360° light distribution
			• Removable Glare Guard™ allows for task lighting
			Rechargeable 8800 mAh Lithium Ion battery
			Portable USB charger:
			8+ full charges: Streamlight Stylus Pro® USB, Clipmate® USB, Double Clutch™ USB
			4 full charges: Streamlight ProTac HL® USB
			4+ full charges: Most smartphones
			2+ full charges: Most tablets
			Stand it upright or hang it with D-ring on base of lantern to use as an overhead light
			Incorporated D-rings on top and bottom of lantern - hang in either inverted or upright positions; spring-loaded to stow against the light, out of the way when not in use
			Ergonomic handle designed to lock in upright or stowed position; incorporated hook allows for hanging on horizontal edges
			Recessed power button prevents accidental actuation; features battery level indicator
			• Battery level indicator changes from green, to yellow, to red, then to flashing red when battery needs to be recharged
			Durable, polymer construction with rubber molded base that provides stability on slippery or uneven surfaces; rubber cover protects charge and USB ports
			Unbreakable polycarbonate lenses
			IPX7 waterproof to 1m submersion; it floats; 2m impact resistance tested
			It floats so you can retrieve it if you drop it in the water
			7.5 in (19.05 cm); 1 lb 14.0 oz (867.62g)
			Includes AC charge cord; optional DC cord available
			Available in yellow or coyote',
				'extract' 		=> 'RECHARGEABLE SCENE LIGHT/WORK LANTERN WITH USB CHARGER
			This rechargeable, 1,100 lumen scene lantern also provides an auxiliary USB power source to charge mobile devices or other Streamlight products. The 8800mAH lithium ion battery capacity gives up to 4 full charges for most smartphones.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/super-siege_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 3
			],
			[
				'name' 			=> 'SIEGE AA',
				'slug' 			=> 'siege-aa',
				'description' 	=> 'Multiple modes:
			White LED (one C4® LED) with cover removed:
			High for brightest light: 200 lumens; runs 7 hours
			Medium for bright light and longer run times: 100 lumens; runs 15.5 hours
			Low provides extended run times for when less light is needed: 50 lumens; runs 37 hours
			Red LED (two red LEDs)
			Red LED High (night vision preserving mode): 0.7 lumens; runs 192 hours (8 days)
			Flash SOS mode for emergency signaling: 0.7 lumens; runs 288 hours (12 days)
			Polycarbonate glare-reducing cover provides soft, even 360° light distribution; comfortable to use in close quarters without impairing vision; cover is removable to illuminate large areas
			Stand it upright or to use as an overhead light or hang it with D-ring on base of lantern
			Incorporated D-ring on bottom of lantern is spring-loaded to stow against the light, out of the way when not in use
			Ergonomic handle designed to lock in upright or stowed position; incorporated hook allows for hanging on horizontal rope, cables and pipes
			Uses three "AA" alkaline batteries (sold separately)
			Keyed battery door facilitates battery replacement in the dark
			Recessed power button prevents accidental actuation; features battery level indicator
			Battery level indicator changes from green, to yellow, to red, then to flashing red when batteries reach the end of their useable life
			Durable, polymer construction with rubber molded base that provides stability on slippery or uneven surfaces
			IPX7 waterproof to 1m submersion; it floats; 2m impact resistance tested
			It floats so you can retrieve it if you drop it in the water
			5.44 in. (13.82 cm); 8.8 oz. (249.47 grams) with batteries
			Available in Coyote
			RoHS compliant',
				'extract' 		=> 'A rugged, compact lantern for when you need a lot of light without it taking up a lot of space. The Siege AA uses affordable, easy-to-find AA alkaline batteries to provide 360° of soft, even light that illuminates a large area.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/the-siege-aa_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 3
			],
			[
				'name' 			=> 'SURVIVOR',
				'slug' 			=> 'survivor',
				'description' 	=> 'Available in rechargeable and non-rechargeable models
			Features C4® LED technology with four lighting modes:
			Rechargeable model:
			High for a super-bright beam that pierces through smoke: 41,000 candela; 175 lumens; 405m beam distance; runs 3.5 hours (lithium ion)
			Low for bright light and longer run time: 14,000 candela; 60 lumens; 237m beam distance; runs 10 hours (lithium ion)
			Flash for signaling: runs 5 hours
			Moonlight mode provides low-level lighting for battery conservation: runs 15 days
			Non-rechargeable model:
			High for a super-bright beam that pierces through smoke: 41,000 candela; 175 lumens; 405m beam distance; runs 8 hours (AA alkaline or AA lithium)
			Low for bright light and longer run time: 14,000 candela; 60 lumens; 237m beam distance; runs 20 hours (AA alkaline or AA lithium)
			Flash for signaling: runs 5 hours
			Moonlight mode provides low-level lighting for battery conservation: runs 15 days
			Custom designed optic with optional, interchangeable Smoke Cutter® plugs for preferred beam pattern:
			Black plug significantly reduces peripheral light for a tighter beam
			Amber plug reduces peripheral light and eye fatigue caused by glare
			By not using either plug, you get the traditional Survivor beam
			Strong spring-loaded clip securely grabs onto belts and gear; D-ring features forward hanging orientation
			Optimized electronics provide regulated intensity
			Industrial rechargeable lithium ion battery can be recharged up to 1,500 times. Also accepts four "AA" alkaline or lithium non-rechargeable batteries
			Rubber dome push-button switch provides easy operation even when wearing heavy gloves; ultrasonic-welded push button switch designed for extremely long life
			High-impact super-tough nylon construction offers exceptional durability; O-ring sealed, unbreakable polycarbonate lens with silicone anti-scratch coating is assembled in a heavy-duty bezel
			IP66 water-resistant; 2 meter impact resistance tested
			Serialized for positive identification
			ATEX-rated for Category 1 (Zone 0) locations. IECEx-rated for Group IIC locations. For use in places with an explosive gas atmosphere (other than mines susceptible to firedamp) that is present continually or for longer periods. It has a maximum adverse surface temperature of <135°C.
			Rated for Category 2G (Zone 1) explosive gas Group IIB locations. For use in places with an explosive gas atmosphere (other than mines susceptible to firedamp) that is likely to occur in normal operation occasionally. It has a maximum adverse surface temperature of 135°C.
			17.92 cm; 388g (with lithium rechargeable or AA alkaline); 354g (with AA lithium)
			International Safety Orange',
				'extract' 		=> 'Rated for Zone 0. The redesigned Survivor features a low-profile bezel that doesn’t interfere with gear and equipment. We also redesigned the clip and added a reinforced D-ring so your light hangs forward.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/survivor-atex_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 3
			],
			[
				'name' 			=> 'VANTAGE',
				'slug' 			=> 'vantage',
				'description' 	=> 'Lightweight, compact, low profile design easily attaches/detaches above or below brim on fire helmets, industrial helmets and hard hats
			C4® LED delivers 7,000 candela peak beam intensity; 115 lumens; 167 meter beam distance; runs 6 hours
			Features an ultra-bright blue taillight LED (green model features an ultra-bright green taillight LED)
			Fits both traditional and modern style helmets
			Tighten clamp with your fingers. No tools necessary!
			Swivel clamp and insertable 5° angle adjuster to optimize beam location
			Rotates 360 degrees on clamp
			Textured parabolic reflector produces a smooth beam hotspot with optimum peripheral illumination
			Anodized aircraft aluminum construction with high temperature, shock mounted, impact resistant Boro Float glass lens
			High-impact, chemical-resistant polymer LED housing with reflective strip
			IPX7 waterproof to 1 meter for 30 minutes
			Uses two 3V CR123A lithium batteries (included)
			3.26 in. (8.28 cm); 5.14 oz (145.7 grams)
			Limited Lifetime Warranty
			Available in black with blue LED or green with green LED
			Assembled in USA',
				'extract' 		=> 'The solution to firemen’s hands-free lighting needs. Compact, powerful, shock-proof, and virtually indestructible!',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/vantage-2011_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'KNUCKLEHEAD HAZ-LO MODELS',
				'slug' 			=> 'knucklehead-haz-lo-models',
				'description' 	=> 'Features 2 C4® LEDs within an engineered reflector that produce a smooth flood beam to illuminate your work area
			Features a 210 degree articulating head that rotates a full 360 degrees
			4 lighting modes:
			High for a soft flood beam; 163 lumens; 730 candela, runs 3.75 hours
			Low for when less light is ideal and for longer run times: 50 lumens; 230 candela; runs 12 hours
			Moonlight for low-level lighting and extended run times - runs 20 days
			Flash for signaling - runs 8 hours
			Optimized electronics provide regulated intensity
			Dual power source: accepts rechargeable NiCd battery pack or 4 "AA" alkaline batteries
			Rubber dome push-button actuator is easy to operate even when wearing heavy gloves
			Removable magnet with 135 lb pull strength features a replaceable rubber boot
			Available in yellow or orange:
			Yellow model features an integrated, stowable hook for hands-free use
			Orange model features a durable, spring-loaded clip that clamps to objects or turn-out gear for hands-free use
			High-impact, super-tough nylon polymer construction for exceptional durability
			2 meter impact-resistance tested
			IPX4 rated for water-resistance
			Serialized for positive identification
			9.2 in. (23.4 cm); NiCd 23.1 oz (655 grams); Alkaline 18.3 oz (519 grams)
			Limited lifetime warranty
			Assembled in USA',
				'extract' 		=> 'Add this feature-laden, safety approved worklight to your toolbox of intrinsically safe equipment. It provides a soft flood pattern to safely illuminate your work area.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/knucklehead-hazlo-flood_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'KNUCKLEHEAD FLOOD MODELS',
				'slug' 			=> 'knucklehead-flood-models',
				'description' 	=> 'Features 2 C4® LEDs within an engineered reflector that produce a smooth flood beam to illuminate your work area
			Features a 210 degree articulating head that rotates a full 360 degrees
			4 lighting modes:
			High for a soft flood beam; 163 lumens; 730 candela, runs 3.75 hours
			Low for when less light is ideal and for longer run times: 50 lumens; 230 candela; runs 12 hours
			Moonlight for low-level lighting and extended run times - runs 20 days
			Flash for signaling - runs 8 hours
			Optimized electronics provide regulated intensity
			Dual power source: accepts rechargeable NiCd battery pack or 4 "AA" alkaline batteries
			Rubber dome push-button actuator is easy to operate even when wearing heavy gloves
			Removable magnet with 135 lb pull strength features a replaceable rubber boot
			Available in yellow or orange:
			Yellow model features an integrated, stowable hook for hands-free use
			Orange model features a durable, spring-loaded clip that clamps to objects or turn-out gear for hands-free use
			High-impact, super-tough nylon polymer construction for exceptional durability
			2 meter impact-resistance tested
			IPX4 rated for water-resistance
			Serialized for positive identification
			9.2 in. (23.4 cm); NiCd 23.1 oz (655 grams); Alkaline 18.3 oz (519 grams)
			Limited lifetime warranty
			Assembled in USA',
				'extract' 		=> 'Add this feature-laden, safety approved worklight to your toolbox of intrinsically safe equipment. It provides a soft flood pattern to safely illuminate your work area.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/knucklehead-hazlo-flood_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'CLIPMATE USB',
				'slug' 			=> 'clipmate-usb',
				'description' 	=> 'Features a white C4® LED and a 620-630nm red LED
			Multiple modes:
			C4 LED for white light:
			White C4 LED – High: 70 lumens; 500 candela; runs 3.5 hours
			White C4 LED – Low: 10 lumens; 50 candela; runs 24 hours
			Red LED preserves night vision:
			Red LED – High:0 .5 lumens; 9 candela; runs 16 hours
			Red LED – Low: 0.2 lumens; 3 candela; runs 65 hours
			Lithium Polymer cell fully recharges in-product in 2.5 hours; rechargeable up to 300 times
			Charge indication LED:  LED steady red – charging; LED blinking red – charged
			IPX4 water-resistant; 1 meter impact resistance tested
			Remove the tethered USB cover to expose the charge tab for charging via USB source or AC wall adapter.
			Polymer housing makes the light extremely durable, abrasion resistant and light weight.
			Push button switch cycles through low, high and off; turn light on then press and hold the button to change from white LED to red LED
			3.2 in. (81 mm); 1.9 oz (53.8 grams) with battery
			Charge with Streamlights EPU-5200 portable USB charger (sold separately)
			Limited lifetime warranty',
				'extract' 		=> 'A multi-function, USB rechargeable clip-on light that grabs onto pockets or hats for hands-free use. No need to worry about finding a cord to charge it, as it features a built-in USB charge tab. ',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/clipmate-usb_logoed.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 4
			],
			[
				'name' 			=> 'EPU-5200',
				'slug' 			=> 'epu-5200',
				'description' 	=> '5,200 mAh of power! Provides capacity to recharge a multitude of devices with micro-USB input:
			4+ full charges for Streamlight Stylus Pro® USB, Clipmate® USB and
			Double Clutch™ USB
			2 full charges for Streamlight ProTac HL® USB
			1+ full charges for most smartphones and tablets: 2-3 full charges for iPhones; 2 full charges for Samsung Galaxy Smartphones or most other smartphones
			Built for professional use - high-impact, super-tough, nylon polymer construction; removeable pocket clip
			Cover snaps in place to protect USB connector - sealed for water-resistance (IPX4 water-resistant); power button is protected from inadvertent actuation; cover tethered to prevent loss
			Features charge status LED indicator surrounding the power button
			Durable lithium ion battery rechargeable up to 500 times; fully recharges in 8 hours; very efficient with low self-discharge (LSD) - holds charge up to 3 months
			Removeable pocket clip grabs securely onto belts, pockets, MOLLE, etc.
			White 5mm LED for use as a back up flashlight
			Includes 5” (12.7cm) 2.0 USB to micro-USB cord
			4.45 in (11.3 cm); 5.4 oz (153 grams)',
				'extract' 		=> 'Instant power for everyone on-the-go. Perfect for extended operations/scene management, traveling or whenever you need to charge your Streamlight USB rechargeable flashlights or mobile devices. It’s also a flashlight to light your way.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/epu-5200_angle1.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 4
			],
			[
				'name' 			=> 'CLIPMATE',
				'slug' 			=> 'clipmate',
				'description' 	=> 'Powered by 3 "AAA" alkaline batteries
Three high-intensity, 100,000-hr. life LEDs
27 lumens typical (white LEDs)
Up to 40 hrs. run time
Includes elastic strap headband and lanyard
3.52" long
2.4 oz.
Available in yellow with three white LEDs or black with a choice of white or green LEDs',
				'extract' 		=> 'Equipped with three high-intensity LEDs and an innovative 360º rotating head, this is one versatile little light.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/clipmate.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 4
			],
			[
				'name' 			=> 'THE LOGO',
				'slug' 			=> 'the-logo',
				'description' 	=> 'Co-branding: Reverse side is imprintable with your corporate identity; Imprint area approx .63” x .38”
Perfect for personalization to use as corporate gifts, trade shows, business meetings and more
High-intensity white LED with multiple modes:
- High for brightest light - 10 lumens
- Medium for bright light and longer run times - 5 lumens
- Low provides extended run times for when less light is needed - 2.5 lumens
- Blink to signal your location
High, Medium and Low modes run for several hours of intermittent use; Blink mode will flash continuously for 45 hours
Light features an Auto Off Warning to help conserve batteries
- The light will flash after 4 minutes and turn off after 6
Using the light:
- Press center quickly for on, off and mode changes
- Press once for High, twice for Medium, three times for Low, and four times for Blink
- Press and hold for momentary mode
Uses two 3V CR2016 lithium coin cell batteries (included)
Durable, polymer construction
Weather-resistant; 1 meter impact resistance tested
1.8 in. (45.7 mm); 0.37 oz (10.7 grams) with batteries
Limited Lifetime Warranty
',
				'extract' 		=> 'Brand-enthusiast key chain light for personal safety. This co-branding, imprintable light attaches to key rings, zippers, back packs, etc.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/ther-logo_angle.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 4
			],
			[
				'name' 			=> 'KEY-MATE',
				'slug' 			=> 'key-mate',
				'description' 	=> 'Easily attaches/detaches to just about anything with convenient pocket clip or key ring
Up to 96 hrs. run time
Includes neck lanyard
O-ring sealed for water-resistance
Powered by 4 alkaline button cells (included)
100,000 hr. lifetime high-intensity LED with reflector optics
LED available in white (10 lumens) or green (2.6 lumens)
2.36"
.9 oz.
Available in black or titanium with white or green LEDs and Realtree® Camo with green LED',
				'extract' 		=> 'The world’s smallest, brightest, one-ounce, machined aluminum, one LED flashlight! The Key-Mate® runs longer and stays brighter.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/keymate.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 4
			],
			[
				'name' 			=> 'NANO LIGHT',
				'slug' 			=> 'nano-light',
				'description' 	=> 'Easily attaches/detaches to just about anything with convenient pocket clip or key ring
Up to 8 hrs. run time
Machined aircraft-grade aluminum with anodized finish
Powered by 4 alkaline button cells (included)
100,000 hr. lifetime high-intensity LED
LED available in white (10 lumens)
1.47" x .51"
.36 oz.
Available in black',
				'extract' 		=> 'Truly tiny, the Nano Light® is a weatherproof, personal flashlight featuring a 100,000 hour life LED. Includes a non-rotating snap hook for easy one handed operation when attached to a keychain.',
				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/nano-light.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 4
			],

		);

		Product::insert($data);

	}

}
