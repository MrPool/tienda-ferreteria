<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use  Faker\Factory as Faker;
use App\Products2;

class Products2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      $data = array(
        [
          'name' 			=> 'STINGER LED HL',
          'slug' 			=> 'stinger-led-hl',
          'description' 	=> 'Multi-function On/Off push-button switch lets you choose three lighting modes and strobe. Three modes and strobe:
  High for a super-bright beam - 640 lumens; 22,000 candela peak beam intensity; 297 meter beam distance; runs 1.25 hours
  Medium for bright light and longer run times – 340 lumens; 11,000 candela peak beam intensity; 210 meter beam distance; runs 2 hours
  Low for light without glare and extended run times – 170 lumens; 5,500 candela peak beam intensity; 148 meter beam distance; runs 4 hours
  Strobe for disorienting or signaling your location; runs 3 hours. Deep-dish parabolic reflector produces a concentrated beam with optimum peripheral illumination. C4® LED technology, impervious to shock with a 50,000 hour lifetime
  Optimized electronics provide regulated intensity throughout battery charge
  3-cell, 3.6 Volt Ni-MH sub-C battery, rechargeable up to 1000 times
  10 hr. steady charge, 2.5 hr. fast charge, or PiggyBack charger options
  Machined aircraft aluminum body with non-slip rubberized comfort grip; anti-roll rubber ring included
  IPX4 water-resistant; 1 meter impact resistance tested
  Unbreakable polycarbonate lens with scratch-resistant coating
  8.41” (21.36 cm); 12.4 oz (352 g)
  Serialized for positive identification
  Limited lifetime warranty
  Assembled in USA',
          'extract' 		=> 'When you need maximum illumination with a wide beam to search a large area, the Stinger LED HL rechargeable, high lumen flashlight provides a blast of 640 lumens with 297 meters of beam distance.',
          'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
          'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/stinger-led-hl_logoed.jpg',
          'visible' 		=> 1,
          'created_at' 	=> new DateTime,
          'updated_at' 	=> new DateTime,
          'category_id' 	=> 1
        ],
        [
  				'name' 			=> 'TRIDENT HAZ-LO',
  				'slug' 			=> 'trident-haz-lo',
  				'description' 	=> 'Meets lighting needs for both close-up and distance work
  			Features center C4® LED and three ultra-bright white 5mm LEDs with two output modes:
  			3 ultra-bright white 5mm LEDs for intense brightness and longer run time: 30 lumens; 300 candela peak beam intensity; 35m beam distance; runs 24 hours
  			C4 LED for super-bright light with strong beam distance: 85 lumens; 2,600 candela peak beam intensity; 102m beam distance; runs 8 hours
  			Push-button switch is easy to use when wearing gloves and is recessed to protect it from inadvertent activation
  			Powered by 3 “AAA” alkaline batteries (included)
  			90º tilting head features a robust ratcheting system; prevents neck fatigue
  			Durable, thermoplastic construction with elastomer over mold
  			IPX4 rated for water resistance; 2 meter impact resistance tested
  			Includes elastic head strap and rubber hard hat strap
  			5.5 oz. (156 g)',
  				'extract' 		=> 'When you need a Div. 1 headlight that provides you with the broadest range of lighting applications, the multi-purpose Trident HAZ-LO provides a good mix of light for both distance and up-close work.',
  				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
  				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/trident-hazlo_logoed.jpg',
  				'visible' 		=> 1,
  				'created_at' 	=> new DateTime,
  				'updated_at' 	=> new DateTime,
  				'category_id' 	=> 2
  			],
        [
  				'name' 			=> 'SIEGE AA',
  				'slug' 			=> 'siege-aa',
  				'description' 	=> 'Multiple modes:
  			White LED (one C4® LED) with cover removed:
  			High for brightest light: 200 lumens; runs 7 hours
  			Medium for bright light and longer run times: 100 lumens; runs 15.5 hours
  			Low provides extended run times for when less light is needed: 50 lumens; runs 37 hours
  			Red LED (two red LEDs)
  			Red LED High (night vision preserving mode): 0.7 lumens; runs 192 hours (8 days)
  			Flash SOS mode for emergency signaling: 0.7 lumens; runs 288 hours (12 days)
  			Polycarbonate glare-reducing cover provides soft, even 360° light distribution; comfortable to use in close quarters without impairing vision; cover is removable to illuminate large areas
  			Stand it upright or to use as an overhead light or hang it with D-ring on base of lantern
  			Incorporated D-ring on bottom of lantern is spring-loaded to stow against the light, out of the way when not in use
  			Ergonomic handle designed to lock in upright or stowed position; incorporated hook allows for hanging on horizontal rope, cables and pipes
  			Uses three "AA" alkaline batteries (sold separately)
  			Keyed battery door facilitates battery replacement in the dark
  			Recessed power button prevents accidental actuation; features battery level indicator
  			Battery level indicator changes from green, to yellow, to red, then to flashing red when batteries reach the end of their useable life
  			Durable, polymer construction with rubber molded base that provides stability on slippery or uneven surfaces
  			IPX7 waterproof to 1m submersion; it floats; 2m impact resistance tested
  			It floats so you can retrieve it if you drop it in the water
  			5.44 in. (13.82 cm); 8.8 oz. (249.47 grams) with batteries
  			Available in Coyote
  			RoHS compliant',
  				'extract' 		=> 'A rugged, compact lantern for when you need a lot of light without it taking up a lot of space. The Siege AA uses affordable, easy-to-find AA alkaline batteries to provide 360° of soft, even light that illuminates a large area.',
  				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
  				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/the-siege-aa_logoed.jpg',
  				'visible' 		=> 1,
  				'created_at' 	=> new DateTime,
  				'updated_at' 	=> new DateTime,
  				'category_id' 	=> 3
  			],
        [
  				'name' 			=> 'KEY-MATE',
  				'slug' 			=> 'key-mate',
  				'description' 	=> 'Easily attaches/detaches to just about anything with convenient pocket clip or key ring
  Up to 96 hrs. run time
  Includes neck lanyard
  O-ring sealed for water-resistance
  Powered by 4 alkaline button cells (included)
  100,000 hr. lifetime high-intensity LED with reflector optics
  LED available in white (10 lumens) or green (2.6 lumens)
  2.36"
  .9 oz.
  Available in black or titanium with white or green LEDs and Realtree® Camo with green LED',
  				'extract' 		=> 'The world’s smallest, brightest, one-ounce, machined aluminum, one LED flashlight! The Key-Mate® runs longer and stays brighter.',
  				'price' 		=> $faker->randomFloat($nbMaxDecimal = 2),
  				'image' 		=> 'http://www.streamlight.com/images/default-source/product-large-images/keymate.jpg',
  				'visible' 		=> 1,
  				'created_at' 	=> new DateTime,
  				'updated_at' 	=> new DateTime,
  				'category_id' 	=> 4
  			],
      );

  		Products2::insert($data);

  	}

  }
