<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database see ds.
     *
     * @return void
     */
    public function run()
    {
      $data = array(
    [
      'name' 		  => 'Pedro',
      'last_name' => 'Pool',
      'email' 	  => 'pedropool13@gmail.com',
      'user' 		  => 'poolmoo',
      'password' 	=> \Hash::make('123456'),
      'type' 		  => 'admin',
      'active' 	  => 1,
      'address' 	=> 'Fresno Sur 6, Monterrey, N.L.',
      'created_at'=> new DateTime,
      'updated_at'=> new DateTime
    ],
    [
      'name' 		  => 'Adela',
      'last_name' => 'Torres',
      'email' 	  => 'adela@correo.com',
      'user' 		  => 'adela',
      'password' 	=> \Hash::make('123456'),
      'type' 		  => 'user',
      'active' 	  => 1,
      'address' 	=> 'Tonala 321, Jalisco',
      'created_at'=> new DateTime,
      'updated_at'=> new DateTime
    ],

  );

  User::insert($data);
    }
}
