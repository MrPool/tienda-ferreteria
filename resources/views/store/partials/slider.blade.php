<div id="slider" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#slider" data-slide-to="0" class="active"></li>
    <li data-target="#slider" data-slide-to="1"></li>
    <li data-target="#slider" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="http://i.imgur.com/d7Pk4oy.jpg" alt="slide1">
        <div class="carousel-caption">

        </div>
      </div>
      <div class="item">
        <img src="http://i.imgur.com/Ff1UlbA.jpg" alt="slide2">
        <div class="carousel-caption">

        </div>
      </div>
      <div class="item">
        <img src="http://i.imgur.com/AoIjBi4.jpg" alt="slide3">
        <div class="carousel-caption">

        </div>
      </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
    <span class="" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#slider" role="button" data-slide="next">
    <span class="" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div><hr>
